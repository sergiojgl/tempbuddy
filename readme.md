# Initial problem

Provide an API endpoint that receives a list of workers and a list of shifts and returns the optimal list of matchings. **A list of matchings is optimal if each worker is paired with at least one shift**.

# Solution

## Data Structure

As problem definition didn’t specified a data structure for JSON response, a minimal answer is generated, including two fields:

- **shift**: ID of the shift
- **worker**: ID of the matched worker

All the matchings are objects contained within an array.

## RESTful API

> ``GET /api/matching``

### Introduction

This endpoint return an array of objects containing the optimal match for the given workers and shifts.

### Overview

It normally returns a status code 200 answer with a JSON body. In case a matching is impossible, the answer will be a status code 400, with an error message, always in JSON format (``Content-Type: application/json``).

### Authentication

This API doesn't need any form of authentication

### Error Codes

- **400**: When matching is impossible or when the input is not a valid JSON string
- **500**: Algorithm cannot performs the task

### Rate limit

The API has a limit of 60 request per hour. Remaining requests can be checked in the headers of each response.

# Test Server

The REST API is deployed as a dockerized application in Heroku. You can access this API from this URL:

[https://sergiojgl-tenejob.herokuapp.com/api/matching](https://sergiojgl-tenejob.herokuapp.com/api/matching)

**NOTE**: Access through web browser result in a status code 400 – Bad Request response from server, as the request made has not the correct Content-Type header specified.

**NOTE 2**: When an app on Heroku has only one web dyno and that dyno doesn't receive any traffic in 1 hour, the dyno goes to sleep. When someone accesses the app, the dyno manager will automatically wake up the web dyno to run the web process type. This causes a short delay for this first request, but subsequent requests will perform normally.