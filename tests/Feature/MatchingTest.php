<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MatchingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testRequiresBody()
    {
        $this->json('GET', 'api/matching')
            ->assertStatus(400)
            ->assertJson([
                'error' => 'Bad Request'
            ]);
    }

    public function testRequiresBodyWithWrongContent()
    {
        $payload = file_get_contents(resource_path('tests/badstructure.json'));
        $this->call(
                'GET',
                'api/matching',
                [],
                [],
                [],
                $headers = [
                    'HTTP_CONTENT_LENGTH' => mb_strlen($payload, '8bit'),      
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                $json = $payload
            )
            ->assertStatus(400)
            ->assertJson([
                'error' => 'Bad Request'
            ]);
    }

    public function testRequiresBodyWithRightContent()
    {
        $payload = file_get_contents(resource_path('tests/goodstructure.json'));
        $this->call(
                'GET',
                'api/matching',
                [],
                [],
                [],
                $headers = [
                    'HTTP_CONTENT_LENGTH' => mb_strlen($payload, '8bit'),      
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                $json = $payload
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'shift',
                    'worker'
                ]
            ]);
    }

    public function testImposibleMatching()
    {
        $payload = file_get_contents(resource_path('tests/impossiblematching.json'));
        $this->call(
                'GET',
                'api/matching',
                [],
                [],
                [],
                $headers = [
                    'HTTP_CONTENT_LENGTH' => mb_strlen($payload, '8bit'),      
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                $json = $payload
            )
            ->assertStatus(400)
            ->assertJson([
                'error' => 'Matching is impossible'
            ]);
    }

    public function testCorrectMatching()
    {
        $payload = file_get_contents(resource_path('tests/correctmatching.json'));
        $this->call(
                'GET',
                'api/matching',
                [],
                [],
                [],
                $headers = [
                    'HTTP_CONTENT_LENGTH' => mb_strlen($payload, '8bit'),      
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                $json = $payload
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'shift',
                    'worker'
                ]
            ])
            ->assertJson([
                [
                    "shift" => 1,
                    "worker" => 2
                ],
                [
                    "shift" => 2,
                    "worker" => 1
                ],
                [
                    "shift" => 3,
                    "worker" => 3
                ]
            ]);
    }
}
