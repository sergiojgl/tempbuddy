<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

class MatchingController extends Controller
{
    public function match(Request $request){

        // get Body content and parse JSON
        $json_input = $request->getContent();
        $data = json_decode($json_input, true);
        if(!$data) return response()->json(['error' => 'Bad Request'], 400);

        // Create validator to check structure integrity
        $validator = Validator::make((array)$data, [
            'workers' => 'required|array|min:1',
            'workers.*.id' => 'required|distinct|integer',
            'workers.*.payrate' => 'sometimes',
            'workers.*.availability' => 'required|array|min:1',
            'workers.*.availability.*' => 'required|string|in:Monday,Tuesday,Wednesday,Thursday,Friday',
            'shifts' => 'required|array|min:1',
            'shifts.*.id' => 'required|distinct|integer',
            'shifts.*.day' => 'required|array|min:1',
            'shifts.*.day.*' => 'required|string|in:Monday,Tuesday,Wednesday,Thursday,Friday'

        ]);
        if($validator->fails()) return response()->json(['error' => 'Bad Request', 'valitation' => $validator->errors()], 400);

        $workers = $data['workers'];
        $shifts = $data['shifts'];


        // sort availability by day
        $availability = ['Monday' => 0, 'Tuesday' => 0, 'Wednesday' => 0, 'Thursday' => 0, 'Friday' => 0];
        foreach($workers as &$worker){
            foreach($worker['availability'] as $av)
                $availability[$av] += 1;
            $worker['shift'] = -1;
        }

        $needs = ['Monday' => 0, 'Tuesday' => 0, 'Wednesday' => 0, 'Thursday' => 0, 'Friday' => 0];
        foreach($shifts as &$shift){
            foreach($shift['day'] as $d)
                $needs[$d] += 1;
            $shift['worker'] = -1;
        }

        unset($worker, $shift);

        // if availability is less than needs for one day, matching is impossible
        foreach($needs as $day=>$need)
            if($need > $availability[$day])
                return response()->json(['error' => 'Matching is impossible'], 400);

        asort($availability);
        asort($needs);

        $i = 0;
        $start = 0;
        // days with less availability should be filled before others
        while($i<count($shifts)){
            $shift = $shifts[$i];
            if($shifts[$i]['worker'] == -1){
                $worker = $this->findWorker($workers, $shifts[$i]['day'], $start);
                if($worker == -1){ // not found 
                    return response()->json(['error' => 'Matching is impossible'], 400);
                }else{
                    if($workers[$worker]['shift'] > -1){ // worker need to change shift
                        $shifts[$i]['worker'] = $worker;
                        $aux = $workers[$worker]['shift'];
                        $workers[$worker]['shift'] = $i;
                        $i = $aux;
                        $shifts[$i]['worker'] = -1;
                        $start = $worker+1;
                        $i--;
                    }else{ // worker available
                        $shifts[$i]['worker'] = $worker;
                        $workers[$worker]['shift'] = $i;
                        $start = 0;
                    }
                }
            }
            $i++;
        }

        $result = [];
        foreach($shifts as $shift){
            $match = new \stdClass();
            $match->shift = $shift['id'];
            $match->worker = $workers[$shift['worker']]['id'];
            $result[] = $match;
        }

        return response()->json($result, 200);

        //return response()->json(['error' => 'Bad Request'], 400);
    }

    private function findWorker($workers, $days, $start = 0){
        $used = -1;
        foreach($workers as $w=>$worker){
            if($w >= $start && count(array_intersect($days, (array)$worker['availability'])) == count($days)){
                if($worker['shift'] == -1)
                    return $w;
                else
                    $used = $w;
            }
        }
        return $used;
    }

}
